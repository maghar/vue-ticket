import Vue from 'vue'
import Router from 'vue-router'
import ListItem from '@/components/ListItem'
import ClientComponent from '@/components/ClientComponent'
import SingleItem from '@/components/SingleItem'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/client/:id',
      name: 'client',
      component: ClientComponent
    },
    {
      path: '/list',
      name: 'list',
      component: ListItem,
      props: {
        modeEditOn: true
      }
    },
    {
      path: '/single/:id',
      name: 'SingleItem',
      component: SingleItem
    }

  ]
})
